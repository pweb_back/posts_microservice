const Joi = require("joi");
const dbToInterfacePost = (post) => {
    return {
        id: post.post_id,
        ownerId: post.user_id,
        type: post.posttype,
        category: post.category,
        title: post.title,
        description: post.description,
        imageUrl: post.imageurl,
        upvotes:post.upvotes,
        upvoted:post.upvoted,
        downvoted:post.downvoted,
        interactionCount:post.interactionCount,
        isRetired:post.isretired,
        tags:post.tags
    }
};

function getUserIdFromToken(req) {
    return 'someid';
    const auth = req.header('Authorization');
    const token = auth.replace('Bearer ', '');
    const claimsBase64 = token.split('.')[1];
    const claims = JSON.parse(Buffer.from(claimsBase64, 'base64').toString());
    return claims.sub;
}

const dbToInterfaceOwnedPost = (post) => {
    return {
        id: post.post_id,
        ownerId: post.user_id,
        type: post.posttype,
        category: post.category,
        title: post.title,
        description: post.description,
        imageUrl: post.imageurl,
        upvotes:post.upvotes,
        upvoted:post.upvoted,
        downvoted:post.downvoted,
        interactionCount:post.interactionCount,
        interactions:post.interactions,
        isRetired:post.isretired,
        tags:post.tags,
    }
};

const addPostSchema = Joi.object({
    type: Joi.string().valid('OFFER', 'REQUEST').uppercase().required(),
    category: Joi.string().valid('MATERIALS', 'SHELTER', 'FOOD').uppercase().required(),
    title: Joi.string().required(),
    description: Joi.string().required(),
    image: Joi.required(),
    tags:Joi.array().items(Joi.string())

});

const addTagsSchema = Joi.object({
    tags:Joi.array().items(Joi.string()).required()
});

const removeTagSchema = Joi.object({
    tags:Joi.string().required
});

const addInteractionSchema = Joi.object({
    text: Joi.string().required(),
    phone: Joi.string()
});

const upvotePostSchema = Joi.object({
    upvoted:Joi.bool().required()
});

const downvotePostSchema = Joi.object({
    downvoted:Joi.bool().required()
});


const editPostDescriptionSchema = Joi.object({
    description: Joi.string().required()
});

module.exports = {
    dbToInterfacePost,
    dbToInterfaceOwnedPost,
    addPostSchema,
    addTagsSchema,
    addInteractionSchema,
    removeTagSchema,
    upvotePostSchema,
    downvotePostSchema,
    editPostDescriptionSchema,
    getUserIdFromToken
}
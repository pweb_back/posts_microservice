const { claimIncludes } = require('express-oauth2-jwt-bearer');

module.exports = claimIncludes('https://pweb-app.example.com/roles', 'ADMIN');

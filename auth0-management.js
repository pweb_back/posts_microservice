const axios = require('axios');

const managementAxios = axios.create();

let tokenExpiry;
let managementToken;

async function fetchManagementToken() {
  if (tokenExpiry && Date.now() < tokenExpiry) {
    return managementToken;
  }

  const now = Date.now();

  const response = await axios.create().post('https://dev-vlziad43.eu.auth0.com/oauth/token', {
    client_id: 'RntMg7WoYNyyRW7AsgH3hPO8mXzAuy97',
    client_secret: 'dInt-k3xJqugq9JbVjLP1e45KLe61UTyJhNreB4dndXRwqlTW2yrZsZ7k8YfuB5D',
    audience: 'https://dev-vlziad43.eu.auth0.com/api/v2/',
    grant_type: 'client_credentials',
  });

  tokenExpiry = now + response.data.expires_in * 1000;
  managementToken = response.data.access_token;

  return managementToken;
}

managementAxios.interceptors.request.use(async (config) => {
  config.headers['Authorization'] = `Bearer ${await fetchManagementToken()}`;

  return config;
});

module.exports = managementAxios;

const Router = require('express').Router();
const validator = require('express-joi-validation').createValidator({})
const {
    ServerError
} = require('./errors');

const isOwnerOrAdmin = async (user_id, post_id) => {
    return await isOwner(user_id, post_id);
}

const isOwner = async (user_id, post_id) => {
    const post = await getPostById(post_id);
    console.log("Post : ", post)
    console.log("isowner : ", post.user_id, user_id, post.user_id == user_id)
    return post.user_id == user_id;
}

const {
    getPosts,
    getPostById,
    addPost,
    addTags,
    removeTag,
    upvote,
    downvote,
    interact,
    retirePost,
    deletePost,
    editPostDescription,
    getLatestPost,
    getUserPosts
} = require('./services.js');
const {getUserIdFromToken, addInteractionSchema, editPostDescriptionSchema, downvotePostSchema, upvotePostSchema,
    removeTagSchema, addTagsSchema, addPostSchema
} = require("./utils");

Router.get('/', async (req, res) => {

    const posts = await getPosts();

    res.json(posts);
});

Router.get('/latest', async (req, res) => {

    const post = await getLatestPost();

    res.json(post);
});

Router.get('/get-user-posts', async (req, res) => {
    const user_id = getUserIdFromToken(req);

    const post = await getUserPosts(user_id);

    res.json(post);
});

Router.get('/:id', async (req, res) => {

    const {
        id
    } = req.params;

    const post = await getPostById(id);

    res.json(post);
});

Router.delete('/:id', async (req, res) => {
    const user_id = getUserIdFromToken(req);
    const {
        id
    } = req.params;
    if (!await isOwnerOrAdmin(user_id, id)) {
        res.json(new ServerError("Permission denied. Need to be owner or admin",403))
        return
    }

    const post = await deletePost(id);

    res.json(post);
});

Router.post('/', validator.body(addPostSchema), async (req, res) => {
    const user_id = getUserIdFromToken(req);
    const {
        type,
        category,
        title,
        description,
        image,
        tags
    } = req.body;

    const id = await addPost(user_id, type, category, title, description, image,
        0, false, false, 0, false, tags);

    res.json(id);
});

Router.post('/:id/add-tags', validator.body(addTagsSchema), async (req, res) => {
    const user_id = getUserIdFromToken(req);
    const {
        id
    } = req.params;
    if (!await isOwnerOrAdmin(user_id, id)) {
        res.json(new ServerError("Permission denied. Need to be owner or admin",403))
        return
    }
    const {
        tags
    } = req.body;

    const post = await addTags(id, tags);

    res.json(post);
});

Router.post('/:id/remove-tag', validator.body(removeTagSchema), async (req, res) => {
    const user_id = getUserIdFromToken(req);
    const {
        id
    } = req.params;
    if (!await isOwnerOrAdmin(user_id, id)) {
        res.json(new ServerError("Permission denied. Need to be owner or admin",403))
        return
    }
    const {
        tag
    } = req.body;

    const post = await removeTag(id, tag);

    res.json(null);
});

Router.post('/:id/upvote', async (req, res) => {
    const user_id = getUserIdFromToken(req);
    const {
        id
    } = req.params;

    const post = await upvote(id);

    res.json(null);
});

Router.post('/:id/downvote', async (req, res) => {
    const user_id = getUserIdFromToken(req);
    const {
        id
    } = req.params;

    const post = await downvote(id);

    res.json(null);
});

Router.post('/:id/retire', async (req, res) => {
    const user_id = getUserIdFromToken(req);
    const {
        id
    } = req.params;

    console.log("watafaka", !await isOwnerOrAdmin(user_id, id))
    if (!await isOwnerOrAdmin(user_id, id)) {
        return res.json(new ServerError("Permission denied. Need to be owner or admin",403))
    }
    else {
        const post = await retirePost(id);
        res.json(null);
    }
});

Router.post('/:id/edit-description',  validator.body(editPostDescriptionSchema), async (req, res) => {
    const user_id = getUserIdFromToken(req);
    const {
        id
    } = req.params;
    console.log("User id : ", user_id, "; post id : ", id, "\n isowneradmin : ", await isOwnerOrAdmin(user_id, id))
    if (!await isOwnerOrAdmin(user_id, id)) {
        res.json(new ServerError("Permission denied. Need to be owner or admin",403))
        return
    }
    const {
        description
    } = req.body;

    const post = await editPostDescription(id, description);

    res.json(null);
});

Router.post('/:id/interact', validator.body(addInteractionSchema), async (req, res) => {
    const user_id = getUserIdFromToken(req);
    const {
        id,
    } = req.params;
    console.log("Req data : ")
    console.log(req.body)
    console.log("-----------------------------")
    const {
        text: textProp,
        phone
    } = req.body;

    const post = await interact(id, textProp, phone, user_id);

    res.json(null);
});

module.exports = Router;

// Router.post('/', async (req, res) => {
//
//     const {
//         post_id,
//         user_id,
//         postType,
//         category,
//         title,
//         description,
//         imageUrl,
//         upvotes,
//         upvoted,
//         downvoted,
//         interactionCount,
//         isRetired,
//         tags
//     } = req.body;
//
//
//     const id = await addPost(post_id, user_id, postType, category, title, description, imageUrl,
//         upvotes, upvoted, downvoted, interactionCount, isRetired, tags);
//
//     res.json(id);
// });

module.exports = Router;
const {
    sendRequest
} = require('./http-client');
const {dbToInterfaceOwnedPost, dbToInterfacePost} = require("./utils");

const ioRoute = process.env.IO_SERVICE_API_ROUTE;

const getPosts = async () => {
    console.info(`Sending request to IO for all posts ...`);
    
    const options = {
        url: `http://${ioRoute}/posts`
    }

    const posts = await sendRequest(options);

    return posts.map(dbToInterfacePost);
};

const getLatestPost = async () => {
    console.info(`Sending request to get latest post ...`);

    const options = {
        url: `http://${ioRoute}/posts/latest`
    }

    const post = await sendRequest(options);

    return dbToInterfacePost(post);
};

const getUserPosts = async (user_id) => {
    console.info(`Sending request to get posts of user ${user_id}...`);

    const options = {
        url: `http://${ioRoute}/posts/get-user-posts/${user_id}`
    }

    const posts = await sendRequest(options);

    return posts.map(dbToInterfaceOwnedPost);
};

const getPostById = async (id) => {
    console.info(`Sending request to IO for post ${id} ...`);

    const options = {
        url: `http://${ioRoute}/posts/${id}`
    }

    const post = await sendRequest(options);

    return dbToInterfacePost(post);
};

const addPost = async (user_id, postType, category, title, description, image,
    upvotes, upvoted, downvoted, interactionCount, isRetired, tags) => {
    console.info(`Sending request to IO to add post of user_id ${user_id}, post type ${postType} and category ${category}
     title ${title}, description ${description}, tags ${tags} ...`);
    //Todo : Modificat formatul datelor + rute
    const options = {
        url: `http://${ioRoute}/posts`,
        method: 'POST',
        data: {
            user_id,
            postType,
            category,
            title,
            description,
            image,
            upvotes,
            upvoted,
            downvoted,
            interactionCount,
            isRetired,
            tags
        }
    }

    const id = await sendRequest(options);

    return id;
};

const addTags = async (post_id, tags) => {
    console.info(`Adding tags ${tags} to post ${post_id} ...`);
    const options = {
        url: `http://${ioRoute}/posts/${post_id}/add-tags`,
        method: 'POST',
        data: {
            tags
        }
    }
    const id = await sendRequest(options);
    return id;
};

const removeTag = async (post_id, tag) => {
    console.info(`Removing tag ${tags} from post ${post_id} ...`);
    const options = {
        url: `http://${ioRoute}/posts/${post_id}/remove-tag`,
        method: 'POST',
        data: {
            tag
        }
    }
    const id = await sendRequest(options);
    return id;
};

const upvote = async (post_id) => {
    console.info(`Upvoting post ${post_id} ...`);
    const options = {
        url: `http://${ioRoute}/posts/${post_id}/upvote`,
        method: 'POST',
    }
    const id = await sendRequest(options);
    return id;
};

const downvote = async (post_id) => {
    console.info(`Downvoting post ${post_id} ...`);
    const options = {
        url: `http://${ioRoute}/posts/${post_id}/downvote`,
        method: 'POST',
    }
    const id = await sendRequest(options);
    return id;
};

const interact = async (post_id, text, phone, user_id) => {
    console.info(`Adding interaction ${text}, ${phone} of user ${user_id} to post ${post_id} ...`);
    const options = {
        url: `http://${ioRoute}/posts/${post_id}/interact`,
        method: 'POST',
        data: {
            user_id,
            text,
            phone
        }
    }
    const id = await sendRequest(options);
    return id;
};

const retirePost = async (post_id) => {
    console.info(`Retiring post ${post_id} ...`);
    const options = {
        url: `http://${ioRoute}/posts/${post_id}/retire`,
        method: 'POST',
    }
    const id = await sendRequest(options);
    return id;
};

const deletePost = async (post_id) => {
    console.info(`Deleting post ${post_id} ...`);
    const options = {
        url: `http://${ioRoute}/posts/${post_id}`,
        method: 'DELETE',
    }
    const id = await sendRequest(options);
    return id;
};

const editPostDescription = async (post_id, description) => {
    console.info(`Editing post description of ${post_id} to ${description} ...`);
    const options = {
        url: `http://${ioRoute}/posts/${post_id}/edit-description`,
        method: 'POST',
        data: {
            description
        }
    }
    const id = await sendRequest(options);
    return id;
};

module.exports = {
    getPosts,
    getPostById,
    addPost,
    addTags,
    removeTag,
    upvote,
    downvote,
    interact,
    retirePost,
    deletePost,
    editPostDescription,
    getLatestPost,
    getUserPosts
}